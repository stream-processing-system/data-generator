class Config:
    MIN_TEMP = 24
    MAX_TEMP = 26
    MIN_HUM = 60
    MAX_HUM = 80
    MIN_PRESS = 101000
    MAX_PRESS = 101500

    def __init__(self):
        pass
