import json
import uuid
from time import time


class Event:
    def __init__(self, uuid_sensor, event_type, timestamp, temperature, humidity, pressure):
        self.__uuid = uuid.uuid4()
        self.__uuid_sensor = uuid_sensor
        self.__template = ""
        self.__data = ""
        self.__timestamp = timestamp
        self.__temperature = temperature
        self.__humidity = humidity
        self.__pressure = pressure

        self.load()
        self.create(uuid_sensor, event_type, temperature, humidity, pressure)

    def load(self):
        with open('template/event.json', 'r') as f:
            self.__template = json.dumps(json.load(f))

    def create(self, uuid_sensor, event_type, temperature, humidity, pressure):
        self.__data = self.__template.replace(":uuid", str(self.__uuid), 1)
        self.__data = self.__data.replace(":uuid_sensor", str(uuid_sensor), 1)
        self.__data = self.__data.replace(":event_type", event_type, 1)
        self.__data = self.__data.replace("\":temperature\"", str(temperature), 1)
        self.__data = self.__data.replace("\":humidity\"", str(humidity), 1)
        self.__data = self.__data.replace("\":pressure\"", str(pressure), 1)

        if self.__timestamp != 0:
            self.update()

    def update(self):
        self.__data = self.__data.replace("\":timestamp\"", str(self.__timestamp), 1)

    def get_data(self):
        return self.__data

    def get_uuid_sensor(self):
        return self.__uuid_sensor

    def set_timestamp(self, timestamp):
        self.__timestamp = timestamp
        self.update()

    def get_temperature(self):
        return self.__temperature

    def get_humidity(self):
        return self.__humidity

    def get_pressure(self):
        return self.__pressure
