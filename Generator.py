import random

import numpy as np


class Generator:

    def __init__(self, config):
        self.__config = config

    def generate_data(self, num, s, NUMB_OF_SENSOR):
        temperature_list = []
        humidity_list = []
        pressure_list = []

        counter = 0
        param_t = 900
        param_h = 800
        param_p = 700
        param_t_temp = param_t
        param_h_temp = param_h
        param_p_temp = param_p
        while counter < num:
            if counter % 3600 == 0:
                param_t_temp = round(random.randrange(500, 1000, 1))
                param_h_temp = round(random.randrange(500, 1000, 1))
                param_p_temp = round(random.randrange(500, 1000, 1))

            param_t = self.calc_param(param_t_temp, param_t)
            param_h = self.calc_param(param_h_temp, param_h)
            param_p = self.calc_param(param_p_temp, param_p)

            temperature = round(1.2 * np.sin(counter / param_t + 2)
                                + (self.__config.MAX_TEMP + self.__config.MIN_TEMP) / 2
                                + 0.2 * np.random.randn(), 2)
            humidity = round(12 * np.sin(counter / param_h + 0)
                             + (self.__config.MAX_HUM + self.__config.MIN_HUM) / 2
                             + 2 * np.random.randn(), 1)
            pressure = round(300 * np.sin(counter / param_p + 3.1)
                             + (self.__config.MAX_PRESS + self.__config.MIN_PRESS) / 2
                             + 60 * np.random.randn())

            temperature_list.append(temperature)
            humidity_list.append(humidity)
            pressure_list.append(pressure)

            counter = counter + 1

        print("Sensor", s+1, "/", NUMB_OF_SENSOR," - generation data",end='\r')
        return [temperature_list, humidity_list, pressure_list]

    @staticmethod
    def calc_param(param_temp, param):
        if param_temp > param:
            param = param + 1
        if param_temp < param:
            param = param - 1
        return param
