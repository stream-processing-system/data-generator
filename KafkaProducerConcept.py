import uuid
from datetime import datetime
from time import time, sleep, time_ns, strftime, gmtime

from kafka import KafkaProducer

from Event import Event


class KafkaProducerConcept:
    def __init__(self, host, port, topic):
        self._producer = KafkaProducer(bootstrap_servers=[host + ":" + port], acks="all")
        self._topic = topic
        self._timeout = 10

    def send(self, event):
        key = event.get_uuid_sensor()
        value = event.get_data()
        future = self._producer.send(topic=self._topic, key=str.encode(str(key)), value=str.encode(value))

    def send_all(self, sensors, step):
        counter = 0
        sensor1 = sensors[0]
        for i in range(len(sensor1.get_events())):
            counter = counter + 1
            t_ns = time_ns()
            t = int(t_ns/1000000000)
            for sensor in sensors:
                events = sensor.get_events()
                event = events[i]
                event.set_timestamp(t)
                self.send(event)
                
            e_ns = time_ns()
            self.send(Event(uuid.uuid4(), "NONE", int(time()), 0, 0, 0))
            d = int((e_ns-t_ns)/1000)/1000000
            
            print(datetime.fromtimestamp(int(e_ns/1000000000)).strftime('%H:%M:%S.%f')[:-3], ": delay=", d, "sec", "["+str(len(sensors))+" sensors]")
            if step > d:
                sleep(step-d)

