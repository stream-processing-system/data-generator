import uuid

from Event import Event
from EventType import EventType


class Sensor:
    def __init__(self, data):
        self.__uuid_sensor = uuid.uuid4()
        self.__events = []

        self.create_events(data)

    def create_events(self, data):
        temperature_l = data[0]
        humidity_l = data[1]
        pressure_l = data[2]
        for i in range(len(temperature_l)):
            self.__events.append(Event(
                self.__uuid_sensor,
                EventType.CONTROL,
                0,
                temperature_l[i],
                humidity_l[i],
                pressure_l[i]
                )
            )

    def get_events(self):
        return self.__events
