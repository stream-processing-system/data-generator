from KafkaProducerConcept import *
from Generator import *
from Config import *
from Sensor import *
import sys

def init():

    if len(sys.argv) != 6:
        raise ValueError("Fill required args")  

    TIME_IN_HOURS =  int(sys.argv[1])
    FREQ = int(sys.argv[2])
    STEP =  60/FREQ
    NUMB_OF_SENSOR =  int(sys.argv[3])
    HOST =  sys.argv[4]
    PORT =  sys.argv[5]

    TIME = TIME_IN_HOURS*60*60
    NUMB = int(TIME/STEP)
    
    print("Full time:", str(TIME)+"s", "("+str(TIME_IN_HOURS)+"h)")
    print("Step:", str(STEP)+"s")
    print("Number of sensors:", NUMB_OF_SENSOR)
    print("Number of events per sensor:", NUMB)
    print("Kafka host:", HOST)
    print("Kafka port:", PORT)

    return NUMB_OF_SENSOR, NUMB, STEP, HOST, PORT

def main():
    NUMB_OF_SENSOR, NUMB, STEP, HOST, PORT = init()

    generator = Generator(Config())

    sensors = []
    for s in range(NUMB_OF_SENSOR):
        sensors.append(Sensor(generator.generate_data(NUMB, s, NUMB_OF_SENSOR)))    
    print()
    print()
    kafkaClient = KafkaProducerConcept(HOST, PORT, "input-topic")

    kafkaClient.send_all(sensors, STEP)


if __name__ == "__main__":
    main()
